import { Component, OnInit } from '@angular/core';
import { WikiService } from './wiki.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'wiki';
  text = '';
  page$ = this.wikiService.getPage();

  constructor(private wikiService: WikiService) {}

  ngOnInit(): void {
    this.page$.subscribe((page: any) => {
      this.text = page.parse.text['*'];
      console.log(this.text);
    });
  }
}
