import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class WikiService {
  constructor(private http: HttpClient) {}

  getPage() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': 'http://localhost:4200',
      }),

    };
    return this.http.get(
      'https://en.wikipedia.org/w/api.php?action=parse&prop=text&page=TypeScript&format=json&origin=http://localhost:4200',
      httpOptions,
    );
  }
}
